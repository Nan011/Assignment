package animals;

public class Animal {
	protected String name;
	protected int bodyLength;
	
	public int getLength() {
		return this.bodyLength;
	}

	public String getName() {
		return this.name.substring(0, 1).toUpperCase() + this.name.substring(1);
	}

	public void makesFlySound() {
	}

	public void makesGnawSound() {
	}

	public void makesRunOnWheelSound() {
	}

	public void makesRoarSound() {
	}

	public void makesBrushSound() {
	}

	public void makesAngrySound() {
	}

	public void makesAVoice(String word) {
	}

	public void confuses() {
	}

	public void makesCuddleSound() {
	}
}