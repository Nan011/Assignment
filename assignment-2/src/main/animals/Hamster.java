package animals;

public class Hamster extends Animal {
	public Hamster(String name, int bodyLength) {
		this.name = name.toLowerCase();
		this.bodyLength = bodyLength;
	}

	public void makesGnawSound() {
		System.out.println("ngkkrit.. ngkkrrriiit");
	}

	public void makesRunOnWheelSound() {
		System.out.println("trrr... trrr...");
	}
}