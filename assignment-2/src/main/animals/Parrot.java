package animals;

public class Parrot extends Animal {
	public Parrot (String name, int bodyLength) {
		this.name = name.toLowerCase();
		this.bodyLength = bodyLength;
	}

	public void makesFlySound() {
		System.out.println("FLYYYY.....");
	}

	public void makesAVoice(String word) {
		System.out.println(word.toUpperCase());
	}

	public void confuses() {
		System.out.println("HM?");
	}
}