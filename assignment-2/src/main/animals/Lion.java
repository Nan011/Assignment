package animals;

public class Lion extends Animal{
	public Lion(String name, int bodyLength) {
		this.name = name.toLowerCase();
		this.bodyLength = bodyLength;
	}

	public void makesRoarSound() {
		System.out.println("err...!");
	}

	public void makesBrushSound() {
		System.out.println("Hauhhmm!");
	}

	public void makesAngrySound() {
		System.out.println("HAUHHMM!");
	}
}