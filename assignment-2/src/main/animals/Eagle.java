package animals;

public class Eagle extends Animal{
	public Eagle(String name, int bodyLength) {
		this.name = name.toLowerCase();
		this.bodyLength = bodyLength;
	}

	public void makesFlySound() {
		System.out.println("kwaakk...");
	}
}