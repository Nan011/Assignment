package animals;

public class Cat extends Animal {
	public Cat(String name, int bodyLength) {
		this.name = name.toLowerCase();
		this.bodyLength = bodyLength;
	}

	public void makesBrushSound() {
		System.out.println("Nyaaan...");
	}	

	public void makesCuddleSound() {
		System.out.println("Purrr...");
	}
}