import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashMap;

import animals.*;
import cage.*;

public class JavariParkManager {
	private static Scanner scan = new Scanner(System.in);
	private static int n;
	private static String[] input;
	private static String[] animalNames;
	private static String[] animalTypes = new String[] {"cat", "lion", "eagle", "parrot", "hamster"};

	private static HashMap<Integer, ArrayList<Animal>> animals = new HashMap<Integer, ArrayList<Animal>>();
	private static CageEnvironment[] cagesEnvironment = new CageEnvironment[5];

	public static String[] split(String word, String splitter) {
		int length = word.length();
		ArrayList<String> list = new ArrayList<String>();
		int ix0 = 0;
		int size = 0;
		for (int ix1=0; ix1<length; ix1++) {
			if (word.substring(ix1, ix1+1).equals(splitter)) {
				list.add(word.substring(ix0, ix1));
				ix0 = ix1 + 1;
				size++;
				
			} else if (ix1 + 1 == length) {
				list.add(word.substring(ix0));
				ix0 = ix1 + 1;
				size++;
			}

		}
		return list.toArray(new String[size]);
	}

	public static Cage[][] arrangementFirstStep(Animal[] animals, boolean isIndoorLocation) {
		Cage[][] cages = new Cage[3][];
		if (animals.length < 3) {
			for (int i=0; i<3; i++) {
				cages[i] = new Cage[1];
				if (i >= animals.length) {
					cages[i] = null;
				} else {
					if (isIndoorLocation) {
						cages[i][0] = new Cage(animals[i], categorizeIndoorCage(animals[i].getLength()));
					} else {
						cages[i][0] = new Cage(animals[i], categorizeOutdoorCage(animals[i].getLength()));
					}
				}
			}

		} else {
			int size = animals.length / 3;
			int remainder = animals.length % 3;
			int i = 0;
			int j = 0;
			cages[i] = new Cage[size];
			for (Animal animal: animals) {
				if (isIndoorLocation) {
					cages[i][j++] = new Cage(animal, categorizeIndoorCage(animal.getLength()));
				} else {
					cages[i][j++] = new Cage(animal, categorizeOutdoorCage(animal.getLength()));
				}
				if (j == size) {
					j = 0;
					if (i == 2) {
						break;
					}
					if (i == 2 - remainder) {
						size++;
					}
					cages[++i] = new Cage[size];
				} 
			}
		}
		return cages;
	}

	public static void arrangementSecondStep(Cage[][] cages) {
		Cage[] swap = cages[2];
		reverseData(cages[1]); cages[2] = cages[1];
		reverseData(cages[0]); cages[1] = cages[0];
		reverseData(swap); cages[0] = swap;
	}

	private static void reverseData(Cage[] cages) {
		if (cages == null) {
			return ;
		} else {
			Cage swap;
			for (int i = 0, j = cages.length-1; j > i; i++, j--) {
				swap = cages[j];
				cages[j] = cages[i];
				cages[i] = swap;
			}
		}
	}

	public static String categorizeIndoorCage(int size) {
		if (size < 45) {
			return "A";
		} else if (45 < size && size < 60) {
			return "B";
		} else {
			return "C";
		}
	}

	public static String categorizeOutdoorCage(int size) {
		if (size < 75) {
			return "A";
		} else if (75 < size && size < 90) {
			return "B";
		} else {
			return "C";
		}
	}

	public static void main(String[] args) {
		for (int i = 0; i < 5; i++) {
			animals.put(i, new ArrayList<Animal>());
		}

		System.out.println("Welcome to Javari Park!\nInput the number of animals");
		for (String animalType: animalTypes) {
			System.out.print(animalType + ": ");
			n = Integer.parseInt(scan.nextLine());
			if (n == 0) {
				continue;
			}

			System.out.println("Provide the information of " + animalType + "(s):");
			input = split(scan.nextLine().replaceAll(" ", ""), ",");
			
			for (String name: input) {
				String name0 = split(name, "|")[0];
				int size = Integer.parseInt(split(name, "|")[1]); 
				if (animalType.equals("cat")) {
					animals.get(0).add(new Cat(name0, size));
				} else if (animalType.equals("lion")) { 
					animals.get(1).add(new Lion(name0, size));
				} else if (animalType.equals("eagle")) {
					animals.get(2).add(new Eagle(name0, size));
				} else if (animalType.equals("parrot")) {
					animals.get(3).add(new Parrot(name0, size));
				} else if (animalType.equals("hamster")) {
					animals.get(4).add(new Hamster(name0, size));
				}		
			}
		}

		System.out.println("Animals have been successfully recorded!" + 
			"\n\n=============================================" +
			"\nCage arrangement:"
		);

		for (int i=0; i<5; i++) {
			if (animals.get(i).size() == 0) {
				continue;
			}	
			boolean isIndoorLocation;
			if (i == 1 || i == 2) {
				isIndoorLocation = false;
			} else {
				isIndoorLocation = true;
			}
			String location = (isIndoorLocation)? "indoor": "outdoor";

			cagesEnvironment[i] = new CageEnvironment(arrangementFirstStep(animals.get(i).toArray(new Animal[animals.get(i).size()]), isIndoorLocation), location);
			System.out.println("location: " + location);
			System.out.println(cagesEnvironment[i]);

			arrangementSecondStep(cagesEnvironment[i].getCages());
			System.out.println("After rearrangement...");
			System.out.println(cagesEnvironment[i]);
		}

		System.out.println("NUMBER OF ANIMALS:");
		for (int i=0; i<5; i++) {
			System.out.println(animalTypes[i] + ":" + animals.get(i).size());
		}

		System.out.println("\n=============================================");

		input = new String[3];
		Animal animal = new Cat("Name", 0);
		while (true) {
			boolean haveToRestart = false;
			String animalType;
			int numberType;

			System.out.println("Which animal you want to visit?" + 
				"\n(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: exit)"
			);

			input[0] = scan.nextLine();
			if (input[0].equals("99")) {
				break;
			}

			if (input[0].equals("2")) {
				numberType = 2;
			} else if (input[0].equals("3")) {
				numberType = 4;
			} else if (input[0].equals("5")) {
				numberType = 1;
			} else {
				numberType = Integer.parseInt(input[0])-1;
			}
			animalType = animalTypes[numberType];
			

			System.out.println("Mention the name of " + animalType + " you want to visit: ");
			input[1] = scan.nextLine();
			if (animals.get(numberType).size() != 0) {
				for (int i=0; i<animals.get(numberType).size(); i++) {
					if (input[1].toLowerCase().equals(animals.get(numberType).get(i).getName().toLowerCase())) {
						animal = animals.get(numberType).get(i);
						break;
					} else if (i + 1 == animals.get(numberType).size()) {
						haveToRestart = true;
					}
				}
			} else {
				haveToRestart = true;
			}

			if (haveToRestart) {
				System.out.println("There is no " + animalType + " with that name! Back to the office!\n");
				continue;
			}

			input[1] = input[1].substring(0, 1).toUpperCase() + input[1].substring(1);

			System.out.println("You are visiting " + input[1] + " (" + animalType + ") now, what would you like to do?");
			if (input[0].equals("1")) {
				System.out.println("1: Brush the fur 2: Cuddle");
				input[2] = scan.nextLine();
				if (Integer.parseInt(input[2]) > 2) {
					System.out.println("You do nothing...");
				} else {
					if (input[2].equals("1")) {
						System.out.println("Time to clean "+ input[1] + "'s fur");
					} 
					System.out.print(input[1] + " makes a voice: ");
					if (input[2].equals("1")) {
						animal.makesBrushSound();
					} else if (input[2].equals("2")) {
						animal.makesCuddleSound();
					}
				} 
			} else if (input[0].equals("2")) {
				System.out.println("1: Order to fly");
				input[2] = scan.nextLine();
				if (input[2].equals("1")) {
					System.out.print(input[1] + " makes a voice: ");
					animal.makesFlySound();
					System.out.println("You hurt!");
				} else {
					System.out.println("You do nothing...");
				}
			} else if (input[0].equals("3")) {
				System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
				input[2] = scan.nextLine();
				if (Integer.parseInt(input[2]) > 2) {
					System.out.println("You do nothing...");
				} else {
					System.out.print(input[1] + " makes a voice: ");
					if (input[2].equals("1")) {
						animal.makesGnawSound();
					} else if (input[2].equals("2")) {
						animal.makesRunOnWheelSound();
					}
				}
			} else if (input[0].equals("4")) {
				System.out.println("1: Order to fly 2: Do conversation");
				input[2] = scan.nextLine();
				if (input[2].equals("1")) {
					System.out.println("Parrot Greeny flies!");
					System.out.print(input[1] + " makes a voice: ");
					animal.makesFlySound();
				} else {
					String word = "";
					if (input[2].equals("2")) {
						System.out.print("You say: ");
						word = scan.nextLine();
					} 
					System.out.print(input[1] + " says: ");
					if (input[2].equals("2")) {
						animal.makesAVoice(word);
					}
					else {
						animal.confuses();
					}
				}
			} else if (input[0].equals("5")) {
				System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
				input[2] = scan.nextLine();
				if (Integer.parseInt(input[2]) > 3) {
					System.out.println("You do nothing...");
				} else {
					if (input[2].equals("1")) {
						System.out.println("Lion is hunting..");
					} else if (input[2].equals("2")) {
						System.out.println("Clean the lion's mane..");
					}
					System.out.print(input[1] + " makes a voice: ");
					if (input[2].equals("1")) {
						animal.makesRoarSound();
					} else if (input[2].equals("2")) {
						animal.makesBrushSound();
					} else if (input[2].equals("3")) {
						animal.makesAngrySound();
					} 
				}
			}
			System.out.println("Back to the office!\n");
		}
	}
}