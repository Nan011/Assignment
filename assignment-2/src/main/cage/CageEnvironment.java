package cage;

public class CageEnvironment {
	Cage[][] cages;
	String location;
	public CageEnvironment(Cage[][] cages, String location) {
		this.cages = cages;  
		this.location = location;
	}

	public Cage[][] getCages() {
		return this.cages;
	}

	public String getLocation() {
		return this.location;
	}

	public String toString() {
		String info = "";
		for (int i=0; i<3; i++) {
			String info1 = "level " + (i + 1) + ":";
			if (this.cages[i] == null) {
				info = info1 + "\n" + info;
				continue;
			}
			for (Cage cage: this.cages[i]) {
				info1 += " " + cage + ",";
			}
			info = info1 + "\n" + info;
		} 
		return info;
	}
}