package cage;

import animals.Animal;

public class Cage {
	private Animal animal;
	private String cageType;
	public Cage(Animal animal, String cageType) {
		this.animal = animal;  
		this.cageType = cageType;
	}

	public Animal getAnimal() {
		return this.animal;
	}

	public String toString() {
		return animal.getName() + " (" + animal.getLength() + " - " + this.cageType + ")";  
	}
}