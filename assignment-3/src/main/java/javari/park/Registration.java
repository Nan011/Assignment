package javari.park;

import java.util.ArrayList;

/**
 * Class describes Registration of ticket which selected by visitor
 *
 * @author Nandhika Prayoga
 */
public class Registration {
    private static long idNumber = 1;

    private final long registrationId;
    private final String visitorName;
    private ArrayList<Attraction> selectedAttractions = new ArrayList<>();
    
    /**
     * Contructor of this class
     *
     * @param visitorName   visitor's name
     */
    public Registration(String visitorName) {
        this.registrationId = this.idNumber++;
        this.visitorName = visitorName;
    }

    /**
     * Getter methods
     *
     * @return get instance variable of this class, it depends which method to used
     */
    public long getRegistrationId() {
        return this.registrationId;
    }

    public String getVisitorName() {
        return this.visitorName;
    }
    
    public ArrayList<Attraction> getSelectedAttractions() {
        return this.selectedAttractions;
    }

    /**
     * Method to add more attraction which is selected by visitor
     *
     * @param selectedAttraction    attraction has been selected by visitor
     */
    public void addSelectedAttraction(Attraction selectedAttraction) {
        this.selectedAttractions.add(selectedAttraction);
    }
}
