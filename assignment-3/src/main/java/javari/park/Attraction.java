package javari.park;

import java.util.ArrayList;

import javari.animal.Animal;

/**
 * This class describes property of attraction,
 * contains name, type, and list of the animals
 *
 * @author Programming Foundations 2 Teaching Team
 * @author Nandhika Prayoga
 */
public class Attraction {

    private String attractionName;
    private String animalType;
    private ArrayList<Animal> animals;
    
    /**
    * Contructor of this class
    *
    * @param attractionName     name of the attraction
    * @param animalType         type of the animal
    * @param animals            list of animals
    */
    public Attraction(String attractionName, String animalType, ArrayList<Animal> animals) {
        this.attractionName = attractionName;
        this.animalType = animalType;
        this.animals = animals;
    }

    /**
    * Getter methods
    *
    * @return get instance variable of this class, it depends which method to used
    */

    public String getName() {
        return this.attractionName;
    }

    public String getType() {
        return this.animalType;
    }

    public ArrayList<Animal> getPerformers() {
        return this.animals;
    }
}
