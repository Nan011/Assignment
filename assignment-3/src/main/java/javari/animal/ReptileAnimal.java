package javari.animal;

/**
 * This class describes expected behaviours of Reptile's animal
 * This class can show us special condition and showable of the certain animal
 * @author Programming Foundations 2 Teaching Team
 * @author Nandhika Prayoga
 */
public class ReptileAnimal extends Animal {
	private boolean isTame;

    /**
     * Constructs an instance of {@code ReptileAnimal}.
     *
     * @param id            unique identifier
     * @param type          type of animal, e.g. Hamster, Cat, Lion, Parrot
     * @param name          name of animal, e.g. hamtaro, simba
     * @param gender        gender of animal (male/female)
     * @param length        length of animal in centimeters
     * @param weight        weight of animal in kilograms
     * @param isLayingEggs  status of specific condition
     * @param condition     health condition of the animal
     */
	public ReptileAnimal(Integer id, String type, String name, Gender gender, double length,
                  double weight, boolean isTame, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.isTame = isTame;
    }

    /**
     * Method to know that the object or animal can perform or not
     *
     * @return return perform status from instance variable of this class
     */
    public boolean isShowable() {
    	return (isTame)? true: false;
    }
}