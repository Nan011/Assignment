package javari.animal;

import javari.animal.Gender;
/**
 * This class describes expected behaviours of MammalAnimal's animal
 * This class can show us special condition and showable of the certain animal
 * @author Programming Foundations 2 Teaching Team
 * @author Nandhika Prayoga
 */
public class MammalAnimal extends Animal {
	private boolean isPregnant;

    /**
     * Constructs an instance of {@code MammalAnimal}.
     *
     * @param id            unique identifier
     * @param type          type of animal, e.g. Hamster, Cat, Lion, Parrot
     * @param name          name of animal, e.g. hamtaro, simba
     * @param gender        gender of animal (male/female)
     * @param length        length of animal in centimeters
     * @param weight        weight of animal in kilograms
     * @param isLayingEggs  status of specific condition
     * @param condition     health condition of the animal
     */
	public MammalAnimal(Integer id, String type, String name, Gender gender, double length,
                  double weight, boolean isPregnant, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.isPregnant = isPregnant;
    }

    /**
     * Method to know that the object or animal can perform or not
     *
     * @return return perform status from instance variable of this class
     */
    public boolean isShowable() {
        if (super.type.equalsIgnoreCase("lion") && super.gender.equals(Gender.FEMALE)) {
            return false;
        } else {
    	   return !this.isPregnant; 
        }
    }
}