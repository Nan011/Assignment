package javari.animal;

/**
 * This class describes expected behaviours of Aves's animal
 * This class can show us special condition and showable of the certain animal
 * @author Programming Foundations 2 Teaching Team
 * @author Nandhika Prayoga
 */
public class AvesAnimal extends Animal {
	private boolean isLayingEggs;

    /**
     * Constructs an instance of {@code AvesAnimal}.
     *
     * @param id            unique identifier
     * @param type          type of animal, e.g. Hamster, Cat, Lion, Parrot
     * @param name          name of animal, e.g. hamtaro, simba
     * @param gender        gender of animal (male/female)
     * @param length        length of animal in centimeters
     * @param weight        weight of animal in kilograms
     * @param isLayingEggs  status of specific condition
     * @param condition     health condition of the animal
     */
	public AvesAnimal(Integer id, String type, String name, Gender gender, double length,
                  double weight, boolean isLayingEggs, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.isLayingEggs = isLayingEggs;
    }

    /**
     * Method to know that the object or animal can perform or not
     *
     * @return return perform status from instance variable of this class
     */
    public boolean isShowable() {
    	return !this.isLayingEggs;
    }
}