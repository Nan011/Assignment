package javari.constants;

/**
 * Interface describes that all constant animal attraction
 * 
 * @author Nandhika Prayoga
 */
public interface AnimalAttractions {
	static final String COF_ANIMAL = "Whale, Lion, Eagle";
	static final String DA_ANIMAL = "Cat, Snake, Parrot, Hamster";
	static final String CM_ANIMAL = "Hamster, Whale, Parrot";
	static final String PC_ANIMAL = "Cat, Hamster, Snake";
	static final String[] ATTRACTION_NAMES = {"Circle of Fire", "Dancing Animals", "Counting Masters", "Passionate Coders"}; 
}