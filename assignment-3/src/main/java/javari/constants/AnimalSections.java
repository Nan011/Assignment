package javari.constants;

/**
 * Interface describes that all constant animal sections
 * 
 * @author Nandhika Prayoga
 */
public interface AnimalSections {
	static final String[] SECTION_NAMES = {"Explore the Mammals", "World of Aves", "Reptilian Kingdom"};
}