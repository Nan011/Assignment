package javari.constants;

/**
 * Interface describes that all constant animal categories
 * 
 * @author Nandhika Prayoga
 */
public interface AnimalCategories {
	static final String MAMMAL_ANIMAL = "Hamster, Lion, Cat, Whale";
	static final String AVES_ANIMAL = "Eagle, Parrot";
	static final String REPTILE_ANIMAL = "Snake";
}