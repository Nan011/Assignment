package javari.reader;
import java.util.ArrayList;
import java.nio.file.Path;
import java.io.IOException;

import javari.animal.*;
import javari.constants.AnimalCategories;

/**
 * This class reads Records file (csv) and analysys it.
 *
 * @author Nandhika Prayoga
 */
public class CsvRecordsReader extends CsvReader implements AnimalCategories {
	private static final long EMPTY_NUMBER = 0;
	private ArrayList<Animal> animals = new ArrayList<>();
	private long[] counter = {EMPTY_NUMBER, EMPTY_NUMBER};

    /**
     * Contructor of this class, and same as the parent class
     *
     * @param file  file path directory that contains records file (csv)
     */
	public CsvRecordsReader(Path file) throws IOException {
        super(file);
    }

    /**
     * Method that can count and adding animal object simultaneously.
     *
     * @return return the number of valid line and invalid line
     */
    public void countValidationRecords() {
    	String[] words;
    	for (String line: super.lines) {
    		words = line.split(COMMA);
    		if (words.length < 8 || 
    			words[0].equals("") || words[1].equals("") ||
    			words[2].equals("") || words[3].equals("") || 
    			words[4].equals("") || words[5].equals("") || 
    			words[7].equals("")
    			) {
    			this.counter[1] += 1;
    		} else {
				try {
					boolean specialCondition;
					if (MAMMAL_ANIMAL.contains(words[1])) {                  // Mammal animal adder
						specialCondition = (words[6].equalsIgnoreCase("pregnant"))? true: false;
						animals.add(new MammalAnimal(Integer.parseInt(words[0]), words[1], words[2], 
									Gender.parseGender(words[3].toLowerCase()), 
									Double.parseDouble(words[4]), Double.parseDouble(words[5]), specialCondition,
									Condition.parseCondition(words[7].toLowerCase())));
    				
					} else if (AVES_ANIMAL.contains(words[1])) {             // Aves animal adder
						specialCondition = (words[6].equalsIgnoreCase("laying eggs"))? true: false;
						animals.add(new AvesAnimal(Integer.parseInt(words[0]), words[1], words[2], Gender.parseGender(words[3].toLowerCase()), 
									Double.parseDouble(words[4]), Double.parseDouble(words[5]), specialCondition,
									Condition.parseCondition(words[7].toLowerCase())));

					} else if (REPTILE_ANIMAL.contains(words[1])) {          // Reptile animal adder
						specialCondition = (words[6].equalsIgnoreCase("tame"))? true: false;
						animals.add(new ReptileAnimal(Integer.parseInt(words[0]), words[1], words[2], Gender.parseGender(words[3].toLowerCase()), 
									Double.parseDouble(words[4]), Double.parseDouble(words[5]), specialCondition,
									Condition.parseCondition(words[7].toLowerCase())));

					} else {
						this.counter[1]++;
					}
					this.counter[0]++;
				} catch (Exception e) {
					this.counter[1]++;
				}
    		}
    	}
    	return ;
    }

    /**
     * Getter method, this method can return list of the animals that has been added
     *
     * @return return all animals
     */
    public ArrayList<Animal> getAnimals() {
    	return this.animals;
    }
	
	public long[] getCounter() {
		return this.counter;
	}
}