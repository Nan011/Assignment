package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * This class represents the base class for reading all lines from a text
 * file that contains CSV data.
 *
 * @author Programming Foundations 2 Teaching Team
 * @author Nandhika Prayoga
 *    
 */
public abstract class CsvReader {
    public static final String COMMA = ",";

    private final Path file;
    protected final List<String> lines;

    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file  path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *     properly
     */
    public CsvReader(Path file) throws IOException {
        this.file = file;
        this.lines = Files.readAllLines(this.file, StandardCharsets.UTF_8);
    }
    
    /**
     * Method to get all lines
     * 
     * @return return all lines
     */
    public List<String> getLines() {
        return lines;
    }
	/**
     * Abstract method to return the number has been count whether valid or not
     *
     */
	public abstract long[] getCounter();

    /**
     * Abstract method to count all valid line, it depends on every class
     *
     */
    public abstract void countValidationRecords();
}
