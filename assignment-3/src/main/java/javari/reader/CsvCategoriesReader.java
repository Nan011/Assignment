package javari.reader;

import java.nio.file.Path;
import java.io.IOException;
import java.util.HashMap;

import javari.constants.AnimalCategories;

/**
 * This class reads categories file (csv) and analysys it.
 *
 * @author Nandhika Prayoga
 */
public class CsvCategoriesReader extends CsvReader implements AnimalCategories {
	private HashMap<String, String> sections = new HashMap<>();
	private HashMap<String, String> categories = new HashMap<>();
	
	private static void addObject(HashMap<String, String> type, String name) {
		if (type.get(name) == null) {
			type.put(name, "is exist");
		} else {
			return ;
		}
	}
	/**
     * This is constructor and same as the parent class
     *
     * @param file	file path directory that contains categories file (csv)
     */
	public CsvCategoriesReader(Path file) throws IOException {
        super(file);
    }

    /**
     * Method that can count animal object.
     *
     * @return return the number of valid line and invalid line
     */
    public void countValidationRecords() {
    	String[] words;
    	for (String line: super.lines) {
    		words = line.split(COMMA);
			if (MAMMAL_ANIMAL.contains(words[0])) {
				if (words[2].equalsIgnoreCase("Explore the Mammals")) {
					addObject(this.sections, "mammal");
				}
				if (words[1].equalsIgnoreCase("mammals")) {
					addObject(this.categories, "mammal");
				}
			} else if (AVES_ANIMAL.contains(words[0])) {
				if (words[2].equalsIgnoreCase("World of Aves")) {
					addObject(this.sections, "aves");
				}
				if (words[1].equalsIgnoreCase("aves")) {
					addObject(this.categories, "aves");
				}
				
			} else if (REPTILE_ANIMAL.contains(words[0])) {
				if (words[2].equalsIgnoreCase("Reptillian Kingdom")) {
					addObject(this.sections, "reptile");
				}
				if (words[1].equalsIgnoreCase("reptiles")) {
					addObject(this.categories, "reptile");
				}
			}
    	}
    	return ;
    }
	
	public long[] getCounter() {
		return new long[] {this.sections.size(), this.categories.size()};
	}
}