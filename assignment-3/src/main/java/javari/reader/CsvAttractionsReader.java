package javari.reader;

import java.nio.file.Path;
import java.io.IOException;
import java.util.HashMap;

import javari.constants.AnimalAttractions;

/**
 * This class reads attraction file (csv) and analysys it.
 *
 * @author Nandhika Prayoga
 */
public class CsvAttractionsReader extends CsvReader implements AnimalAttractions {
	private HashMap<String, String> attractions = new HashMap<>();
	/**
     * Contructor of this class, and same as the parent class
     *
     * @param file	file path directory that contains attraction file (csv)
     */
	public CsvAttractionsReader(Path file) throws IOException {
        super(file);
    }

	private static void addObject(HashMap<String, String> type, String name) {
		if (type.get(name) == null) {
			type.put(name, "is exist");
		} else {
			return ;
		}
	}
    /**
     * Method that can count animal object.
     *
     * @return return the number of valid line and invalid line
     */
    public void countValidationRecords() {
    	String[] words;
    	for (String line: super.lines) {
    		words = line.split(COMMA);
	    	if (COF_ANIMAL.contains(words[0]) && words[1].equalsIgnoreCase(ATTRACTION_NAMES[0]) ||
	    		DA_ANIMAL.contains(words[0]) && words[1].equalsIgnoreCase(ATTRACTION_NAMES[1]) ||
	    		CM_ANIMAL.contains(words[0]) && words[1].equalsIgnoreCase(ATTRACTION_NAMES[2]) ||
	    		PC_ANIMAL.contains(words[0]) && words[1].equalsIgnoreCase(ATTRACTION_NAMES[3])
	    	) {
				this.addObject(attractions, words[1]);
	    	}
    	}
    	return ;
    }
	
	public long[] getCounter() {
		return new long[] {this.attractions.size(), 4 - this.attractions.size()};
	}
}