import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Map;
import java.util.HashMap;

import javari.animal.*;
import javari.park.*;
import javari.reader.*;
import javari.writer.*;
import javari.constants.*;

/**
 * This class will process all registration of attraction in Javari Park,
 * also it will handle validation the inputs
 *
 * @author Nandhika Prayoga
 */
class Register implements AnimalCategories, AnimalSections, AnimalAttractions {
	private static final String DEFAULT_DIRECTORY_INPUT_PATH = "../../../data/%s";
	private static final String DEFAULT_DIRECTORY_OUTPUT_PATH = "../../../output/";
	private static final String[] FILE_TYPE_NAMES = {"attractions", "categories", "records"};
	
	private static Scanner scan = new Scanner(System.in);
	private static CsvReader[] allCsvReader = new CsvReader[3];
	private static Path[] allPath = new Path[3];
	private static HashMap<String, HashMap<String, ArrayList<Animal>>> animals = new HashMap<>();
	private static HashMap<String, String[]> animalAttractions = new HashMap<>(); 
	private static Registration visitorRegistration = null;
	private static String directoryPath;
	private static String choiceInput;
	private static long[] counterInput;
	private static boolean repeat;

	private static final String OPENING_SENTENCE = "Welcome to Javari Park Festival - Registration Service!";

	/**
     * First step method to choose animal kingdoms
     */
	private static void firstStep() {
		System.out.println("\nPlease answer the questions by typing the number." + 
								"Type # if you want to return to the previous menu");
		System.out.format("Javari Park has 3 sections:%n" + 
							"1. %s%n" +
							"2. %s%n" +
							"3. %s%n" +
							"Please choose your preferred section (type the number): ", 
							SECTION_NAMES[0], SECTION_NAMES[1], SECTION_NAMES[2]);
		choiceInput = scan.nextLine();
		System.out.println();

		if (choiceInput.equals("#")) { // Back step
			firstStep();
		} else { // Next step
			secondStep(SECTION_NAMES[Byte.parseByte(choiceInput)-1]); 
		}
	}

	/**
     * Second step after first step method, this method will give visitor animal type choices
     */
	private static void secondStep(String sectionName) {
		byte count = 0; 
		String[] animalTypes = new String[animals.get(sectionName).size()];
		System.out.format("--%s--%n", sectionName);
		for (Map.Entry<String, ArrayList<Animal>> entry: animals.get(sectionName).entrySet()) {
			animalTypes[count] = entry.getKey();
			System.out.format("%d. %s%n", count + 1, animalTypes[count++]);
		}
		System.out.print("Please choose your preferred animals (type the number): ");
		choiceInput = scan.nextLine();
		System.out.println();

		if (choiceInput.equals("#")) { // Back step
			firstStep();
		} else { // Next step
			lastStep(sectionName, animalTypes[Byte.parseByte(choiceInput)-1]); 
		}
	}

	/**
     * last step after second step method, this method will give visitor attraction choices
     */
	private static void lastStep(String sectionName, String animalType) {

		byte count = 1;
		System.out.format("---%s---%n", animalType);
		for (String attraction: animalAttractions.get(animalType)) {
			System.out.format("%d. %s%n", count++, attraction);
		}

		System.out.print("Please choose your preferred animals (type the number): ");
		choiceInput = scan.nextLine();
		System.out.println();

		if (choiceInput.equals("#")) {
			secondStep(sectionName);
		} else {
			ArrayList<Animal> animalsInCategories = animals.get(sectionName).get(animalType);
			String visitorName = "";
			String animalsName = "";
			String attractionName = animalAttractions.get(animalType)[Byte.parseByte(choiceInput)-1];

			if (visitorRegistration == null) {
				System.out.print("Wow, one more step,\n" + 
									"please let us know your name: ");
				visitorName = scan.nextLine();
				
			} else {
				visitorName = visitorRegistration.getVisitorName();
			}

			for (Animal animal: animalsInCategories) {
				if (animal.isShowable()) {
					animalsName += animal.getName() + ", ";
				}
			}

			// Reporting the data of visitor
			animalsName = animalsName.substring(0, animalsName.length()-2);
			System.out.format("%nYeay, final check!%n" +
								"Here is your data, and the attraction you chose:%n" +
								"Name: %s%n" + 
								"Attractions: %s -> %s%n" +
								"With: %s%n%n" +
								"is the data correct? (Y/N): ", 
								visitorName, attractionName, 
								animalType, animalsName);
			choiceInput = scan.nextLine();

			if (choiceInput.equalsIgnoreCase("n")) { // Back step
				firstStep();
			} else {
				if (visitorRegistration == null) {
					visitorRegistration = new Registration(visitorName);
				} 
				visitorRegistration
					.addSelectedAttraction(new Attraction(attractionName, animalType, animalsInCategories));
				
				System.out.print("Thank you for your interest. Would you like to register to other attractions? (Y/N): ");
				choiceInput = scan.nextLine();
				if (choiceInput.equalsIgnoreCase("y")) { // Back to first step if visitor want more attraction to be selected
					firstStep();
				} else {
					return ;
				}
			}
		}
	}

	/**
     * Main method to running or processing all registration until end program
     */
	public static void main(String[] args) {
		// Prepare all data
		System.out.format("%s%n%n", OPENING_SENTENCE);
		System.out.print(" ... Opening default section database from data.");
		directoryPath = DEFAULT_DIRECTORY_INPUT_PATH;
		
		// Ensure the program will be okay if program has pick wrong path
		do {
			try {
				for (byte index = 0; index < 3; index++) {
					String fileName = String.format("animals_%s.csv", FILE_TYPE_NAMES[index]);
					allPath[index] = Paths.get(String.format(directoryPath, fileName));
					if (index == 0) {
						allCsvReader[0] = new CsvAttractionsReader(allPath[0]);
					} else if (index == 1) {
						allCsvReader[1] = new CsvCategoriesReader(allPath[1]);
					} else {
						allCsvReader[2] = new CsvRecordsReader(allPath[2]);
					}
				}
				repeat = false;
			} catch (IOException e) { // To change directory path
				repeat = true;
				System.out.println(" ... File not found or incorrect file!\n");
				System.out.print("Please provide the source data path: ");
				directoryPath = String.format("%s/%%s", scan.nextLine());
			}
		} while (repeat);

		// Inputing and reporting the data
		System.out.println("\n ... Loading... Success... System is populating data...\n");
		allCsvReader[1].countValidationRecords();
		System.out.format("Found %d valid sections and %d invalid sections%n", 
							allCsvReader[1].getCounter()[0], 3 - allCsvReader[1].getCounter()[0]);
		for (byte index = 0; index < 3; index++) {
			allCsvReader[index].countValidationRecords();
			counterInput = allCsvReader[index].getCounter();
			String csvName = String.format("animal %s", FILE_TYPE_NAMES[index]);
			if (index == 1) {
				System.out.format("Found %d valid %s and %d invalid %s%n", 
									counterInput[1], csvName, 3 - counterInput[1], csvName);
			} else {
				System.out.format("Found %d valid %s and %d invalid %s%n", 
									counterInput[0], csvName, counterInput[1], csvName);
			}
			animals.put(SECTION_NAMES[index], new HashMap<>());

		}

		// Grouping all animals based on animal kingdom and animal type
		for (Animal animal: ((CsvRecordsReader) allCsvReader[2]).getAnimals()) {
			String animalType = animal.getType();
			animalType = animalType.substring(0, 1).toUpperCase() + animalType.substring(1).toLowerCase();
			if (MAMMAL_ANIMAL.contains(animalType)) {			// Grouping mammal animal
				if (animals.get(SECTION_NAMES[0]).get(animalType) == null) {
					animals.get(SECTION_NAMES[0]).put(animalType, new ArrayList<>());
				}
				animals.get(SECTION_NAMES[0]).get(animalType).add(animal);
			} else if (AVES_ANIMAL.contains(animalType)) {		// Grouping aves animal
				if (animals.get(SECTION_NAMES[1]).get(animalType) == null) {
					animals.get(SECTION_NAMES[1]).put(animalType, new ArrayList<>());
				}
				animals.get(SECTION_NAMES[1]).get(animalType).add(animal);

			} else if (REPTILE_ANIMAL.contains(animalType)) {	// Grouping reptile animal
				if (animals.get(SECTION_NAMES[2]).get(animalType) == null) {
					animals.get(SECTION_NAMES[2]).put(animalType, new ArrayList<>());
				}
				animals.get(SECTION_NAMES[2]).get(animalType).add(animal);

			} 
		}

		// Grouping all atraction based on animal type
		animalAttractions.put("Cat", new String[] {ATTRACTION_NAMES[1], ATTRACTION_NAMES[3]});
		animalAttractions.put("Lion", new String[] {ATTRACTION_NAMES[0]});
		animalAttractions.put("Eagle", new String[] {ATTRACTION_NAMES[0]});
		animalAttractions.put("Hamster", new String[] {ATTRACTION_NAMES[1], ATTRACTION_NAMES[2], ATTRACTION_NAMES[3]});
		animalAttractions.put("Parrot", new String[] {ATTRACTION_NAMES[1], ATTRACTION_NAMES[2]});
		animalAttractions.put("Whale", new String[] {ATTRACTION_NAMES[0], ATTRACTION_NAMES[2]});
		animalAttractions.put("Snake", new String[] {ATTRACTION_NAMES[3]});

		// Running main program to used by visitor
		System.out.format("%n%s%n", OPENING_SENTENCE);
		firstStep();
		
		// Make registration as a JSON file
		directoryPath = DEFAULT_DIRECTORY_OUTPUT_PATH;
		// Ensure the program will be okay if the directory path doesn't exist
		do {
			try {
				RegistrationWriter.writeJson(visitorRegistration, Paths.get(directoryPath));
				System.out.println("... End of program,");
				repeat = false;
			} catch (IOException e) {
				System.out.print("Please provide the output directory path: ");
				directoryPath = scan.nextLine();
				repeat = true;
			}
		} while (repeat);
	}
}