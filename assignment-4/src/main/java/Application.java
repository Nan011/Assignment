import javax.swing.SwingUtilities;

import games.MemoryGame;
/**
 * Class untuk men-triggered aplikasi game
 * 
 *
 * @author Nandhika Prayoga
 */
public class Application {
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
	      	public void run() {
	        	new MemoryGame("Pair-Matched Memory Game");
	      	}
	    });
	}
}