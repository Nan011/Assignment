package games.card;

import javax.swing.JButton;
import javax.swing.ImageIcon;

/**
 * Class untuk mendefiniskan kartu yang akan dibuat untuk memory game
 *
 * @author Nandhika Prayoga
 */

public class Card extends JButton {
	public static int HORIZONTAL_SIZE_DEFAULT = 100;
	public static int VERTICAL_SIZE_DEFAULT = 100;
	
	private ImageIcon faceUp;
	private ImageIcon faceDown;
	private byte id;

	/**
     * Constructor dari Card
     *
     * @param iconFaceUp   		gambar tampak depan
     * @param iconFaceDown    	gambar tampak belakang
     * @param idGenerator      	sebuah id sebagi ciri khas dari card
     */
	
	public Card(ImageIcon iconFaceUp, ImageIcon iconFaceDown, byte idGenerator) {
		super(iconFaceUp);
		this.faceUp = iconFaceUp;
		this.faceDown = iconFaceDown;
		this.id = idGenerator;
		this.setup(HORIZONTAL_SIZE_DEFAULT, VERTICAL_SIZE_DEFAULT);
	}

	/**
     * Method untuk membuat size dari button ini
     *
     * @param horSize 	ukuran horizontal
     * @param verSize 	ukurang vertikal
     */
	private void setup(int horSize, int verSize) {
		this.setSize(horSize, verSize);
	}

	/**
     * Returns the correct condition enum based on given string representation
     * of a condition.
     *
     * @param card 	kartu yang akan dicek sama atau tidak
     * @return mengembalikan nilai true jika sama, sebaliknya adalah false
     */
	public boolean equals(Card card) {
		if (this.id == card.id) return true;
		else return false;
	}

	/**
     * Method untuk mengubah gambar dari kartu ini
     *
     */
	public void changeFace() {
		if (this.getIcon() == this.faceUp) {
			this.setIcon(this.faceDown);
		} else {
			this.setIcon(this.faceUp);
		}
	}

	/**
     * Method untuk mengecek apakah kartu sedang menghadap ke bawah atau tidak
     *
     * @return mengembalikan nilai true jika kartu menghadap ke bawah, sebaliknya false
     */
	public boolean isFaceDown() {
		if (this.getIcon() == this.faceDown) {
			return true;
		} else {
			return false;
		}
	}
}