package games;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import java.awt.Image;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.*;
import java.util.Random;

import games.card.Card;

/**
 * Badan dari memory game, dimana pembuatan semua komponen yang diperlukan ada di class ini
 *
 * @author Nandhika Prayoga
 */
public class MemoryGame extends JFrame implements ActionListener{
	private static final String DEFAULT_IMAGE_DIRECTORY_PATH = "../../../img/";
	
	private JPanel panel; 
	private JPanel topPanel; 
	private JPanel bottomPanel;
	JButton restartButton;
	JButton exitButton;
	JLabel numberOfTriesLabel;
	
	private Card[] cards;
	private Card[] selectedCards;
	private byte matchedCards;
	private int numberOfTries;
	private static byte idGenerator = 0; // membentuk id baru setiap kartu

	/**
     * Constructor untuk membuat aplikasi memory game
     *
     * @param name 	nama dari aplikasi memory game-nya
     */
	public MemoryGame(String name) {
		super(name);
		this.setup();
	}

	/**
     * Setup awal yang mewakili setup lainnya
     * Didalamnya ada setup untuk aplikasi itu sendiri, deklarasi sebuah variabel, dan setup untuk panel lainnya
     *
     */
	private void setup() {
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(600, 700));
		this.setResizable(false);
		this.matchedCards = 0;
		this.selectedCards = new Card[2];
		this.cards = new Card[36];
		this.panel = new JPanel(new GridLayout(2, 1));
		this.topPanelSetup();
		this.bottomPanelSetup();
		this.panel.add(this.topPanel);
		this.panel.add(this.bottomPanel);
		this.add(panel);
		this.pack();
	}

	/**
     * Method untuk membentuk panel yang bagian atas
     * Diisi oleh inti game itu sendiri, yaitu card-nya
     *
     */
	private void topPanelSetup() {
		this.topPanel = new JPanel(new GridLayout(6, 6));
		this.topPanel.setPreferredSize(new Dimension(600, 700));
		ImageIcon image0, image1;
		for (byte index = 0; index < 18; index++) {
			image0 = MemoryGame.resizeIcon(new ImageIcon(
								DEFAULT_IMAGE_DIRECTORY_PATH + "cover.jpg"), 100, 100);
			image1 = MemoryGame.resizeIcon(new ImageIcon(String.format(
								DEFAULT_IMAGE_DIRECTORY_PATH + "card/img_%d.jpg", index)), 100, 100);
			this.cards[index] = new Card(image0, image1 , idGenerator);
			this.cards[18 + index] = new Card(image0, image1, idGenerator++);
			this.cards[index].addActionListener(this);
			this.cards[18 + index].addActionListener(this);
		}
		this.shuffleCards();		
	}

	/**
     * Method untuk membentuk panel yang bagian bawah
     * Diisi oleh fitur yang membantu player dalam permainan
     * Ada Exit button, Restart Button, dan informasi jumlah pair-clicked
     */
	private void bottomPanelSetup() {
		this.bottomPanel = new JPanel(new GridLayout(1, 3));
		this.bottomPanel.setSize(100, 600);
		this.restartButton = new JButton("Play Again?");
		this.exitButton = new JButton("Exit");

		this.restartButton.addActionListener(this);
		this.exitButton.addActionListener(this);

		this.numberOfTriesLabel = new JLabel();
		this.numberOfTriesLabel.setText("Number of Tries: " + this.numberOfTries);
		this.bottomPanel.add(this.restartButton);
		this.bottomPanel.add(this.exitButton);
		this.bottomPanel.add(this.numberOfTriesLabel); 
	}

	/**
     * Method untuk mengacak semua kartu yang dimiliki oleh objek dari class ini
     */
	private void shuffleCards() {
		this.topPanel.removeAll();
		Random random = new Random();
		int anyIndex;
		Card temp;
		for (byte index = 0; index < 36; index++) {
			anyIndex = random.nextInt(36 - index) + index;
			temp = cards[index];
			this.cards[index] = cards[anyIndex];
			this.cards[anyIndex] = temp;
			this.topPanel.add(cards[index]);
		}
	}

	/**
     * Method untuk melakukan aksi-aksi tertentu
     * Didalamnya ada tombol restart, exit, dan ketika kartu di-click
     */
	public void actionPerformed(ActionEvent e) {
		if (this.restartButton == e.getSource()) {
			this.resetGame();
		} else if (this.exitButton.equals(e.getSource())) {
			System.exit(0);
		} else {
			for (Card card: this.cards) {
				if (card.equals(e.getSource())) {
					if (this.selectedCards[0] == null) {
						this.selectedCards[0] = card;
						card.changeFace();
						return ;
					} else if (card.equals(this.selectedCards[0]) && card.isFaceDown()) {
						return ;
					} else if (this.selectedCards[1] == null) {
						this.selectedCards[1] = card;
						card.changeFace();
						this.numberOfTriesLabel.setText("Number of Tries: " + (++this.numberOfTries));
						break;
					} else {
						this.selectedCards[0].changeFace();
						this.selectedCards[1].changeFace();
						this.selectedCards[0] = card;
						card.changeFace();
						this.selectedCards[1] = null;
						return ;
					}
				} 
			}
			if (this.selectedCards[0].equals(this.selectedCards[1])) {
				this.selectedCards[0].setVisible(false);
				this.selectedCards[1].setVisible(false);
				this.matchedCards += 1;
				if (this.matchedCards == 18) {
					JOptionPane.showMessageDialog(null, "You Win!!");
					this.resetGame();
				}
			}
		}
	}

	/**
     * Method untuk meng-reset game dari awal
     */
	private void resetGame() {
		for (Card card: this.cards) {
			if (card.isVisible() == false) {
				card.setVisible(true);
				card.changeFace();
			}
			if (card.isFaceDown()) {
				card.changeFace();
			}
		}
		this.selectedCards[0] = null;
		this.selectedCards[1] = null;
		this.matchedCards = 0;
		this.numberOfTries = 0;
		this.numberOfTriesLabel.setText("Number of Tries: 0");
		this.shuffleCards();
		this.validate();
	}

	/**
     * Method static yang digunakan untuk mengubah ukuran icon di suatu card agar sesuai pada ukuran tombolnya
     *
     * @param icon 				gambar yang akan diolah
     * @param resizedWidth		ukuran horizontal yang diharapkan
     * @param resizedHeight		ukuran vertikal yang diharapkan
     * @return 					mengembalikan sebuah gambar yang telah diolah
     */
	private static ImageIcon resizeIcon(ImageIcon icon, int resizedWidth, int resizedHeight) {
	    Image img = icon.getImage();  
	    Image resizedImage = img.getScaledInstance(resizedWidth, resizedHeight,  Image.SCALE_SMOOTH);  
	    return new ImageIcon(resizedImage);
	}
}