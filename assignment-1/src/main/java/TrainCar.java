public class TrainCar {
    public static final double EMPTY_WEIGHT = 20.0;     // In kilograms
    public static final double EMPTY_NUMBER = 0.0;      // Zero number
    private WildCat passenger;                          // variable to store cat on the car
    private TrainCar next;                              // Variable to store a partner car
    private double carWeight = this.EMPTY_WEIGHT;     // Initiate total weight
    private double totalMassIndex = this.EMPTY_NUMBER;  // Initiate total mass index

    public TrainCar(WildCat cat) {  // Constructor to make car itself
        this(cat, null);
    }

    public TrainCar(WildCat cat, TrainCar next) {   // Constructor with connected cars
        this.passenger = cat;
        this.next = next;
    }

    public double computeTotalWeight() {    // To compute total weight of this car and subcar recursively
        if (this.next == null) {
            return this.carWeight + this.passenger.getWeight();
        } else {
            return this.carWeight + this.passenger.getWeight() + this.next.computeTotalWeight();
        }
    }

    public double computeTotalMassIndex() {     // To compute total mass index of this car and subcar recursively
        if (this.next == null) {
            return this.passenger.computeMassIndex();
        } else {
            return this.passenger.computeMassIndex() + this.next.computeTotalMassIndex();
        }
    }

    public void printCar() {    // To print all of car includes car itself and subcar of this car recursively
        if (this.next == null) {
            System.out.println("(" + this.passenger.getName() + ")");
        } else {
            System.out.print("(" + this.passenger.getName() + ")--");
            this.next.printCar();
        }
    }
}
