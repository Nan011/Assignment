public class WildCat {

    private String name; // Name of this cat
    private double weight; // In kilograms
    private double length; // In centimeters

    public WildCat(String name, double weight, double length) { // Constructor of this cat
        this.name = name;
        this.weight = weight;
        this.length = length;
    }   

    public String getName() { // Get name attribute
        return this.name;
    }

    public double getWeight() { // Get weight attribute
        return this.weight;
    }

    public double getLength() { // Get length attribute
        return this.length;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setLength(double length) {
        this.length = length;
    }
    public double computeMassIndex() { // To compute mass index of this cat
        return this.weight / (this.length * this.length * 0.0001);
    }
}
