import java.util.Scanner;
public class A1Station {
    public static final double THRESHOLD = 250;             // in kilograms
    public static final double EMPTY_NUMBER = 0;            // Zero number
    private static Scanner scan = new Scanner(System.in);
    private static TrainCar lastCar;                        // Variable to store most recent car
    private static WildCat cat;                             // Variable to store cat on the most recent car
    private static int numberOfCar = (int) EMPTY_NUMBER; 
    private static double averageMassIndex = EMPTY_NUMBER;

    private static String clearSpace(String word) {     // To clear word from space character and return that word
        for (int i=0; i<word.length(); i++) {
            if (word.charAt(i) == ' ' && i != word.length() - 1) {
                word = word.substring(0, i) + word.substring(i + 1);
                i--;
            } else if (word.charAt(i) == ' '){
                word = word.substring(0, i);
                i--;
            }
        }
        return word;
    }

    private static String categorizeMassIndex(double massIndex) {   // Categorize mass index
        if (massIndex < 18.5) {
            return "underweight";
        } else if (18.5 <= massIndex && massIndex < 25.0) {
            return "normal";
        } else if (25.0 <= massIndex && massIndex < 30.0) {
            return "overweight";
        } else {
            return "obese";
        }
    }

    public static void main(String args[]) {    // Main method
    	int n = Integer.parseInt(scan.nextLine());     // Number of cats
    	for (int i=0; i<n; i++) {      // Processing the number of cats
    		String input[] = clearSpace(scan.nextLine()).split(",");
    		cat = new WildCat(input[0], Double.parseDouble(input[1]), Double.parseDouble(input[2]));
    		averageMassIndex += cat.computeMassIndex();

    		if (numberOfCar++ == EMPTY_NUMBER ) {     // Case when the track is empty
    			lastCar = new TrainCar(cat);
    		} else {                                  // Case when the track isn't empty
                lastCar = new TrainCar(cat, lastCar);    
            }    
            
            if (lastCar.computeTotalWeight() > THRESHOLD || i == n - 1) { // Case when the car or cars has/have exceed than threshold, or has been at the end of the execution
    			System.out.println("The train departs to Javari Park");
    			System.out.print("[LOCO]<--");
    			lastCar.printCar();
                averageMassIndex /= (double) numberOfCar;
                System.out.format("Average mass index of all cats: %.2f\n", averageMassIndex);
                System.out.println("In average, the cats in the train are *" + categorizeMassIndex(averageMassIndex) + "*");
                
                // reinitialize the car/cars on the track
                averageMassIndex = EMPTY_NUMBER;
                numberOfCar = (int) EMPTY_NUMBER;
    		} 
    	}
    }
}
